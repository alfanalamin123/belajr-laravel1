<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('register');
    }
    public function kirim(Request $request)
    {
        return view('welcome', [
            'nama' => $request->firstname,
            'nama_belakang' => $request->lastname
        ]);
    }

    
}